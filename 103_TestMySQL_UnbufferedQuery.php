<!DOCTYPE html>
<html>
<head>
	<title>103_TestMySQL_UnbufferedQuery</title>
</head>
<body>

<?php
/* This script will test unbuffered query. */
/* KEYPOINT_01: Get a result resource form the MySQL server.
   KEYPOINT_02: Fetch the a row from the result set. 
   KEYPOINT_03: Close the resource after using it. */

$mysqli = new mysqli("127.0.0.1:3306","yaoyu","essn9002","menagerie");

if ($mysqli->connect_errno)
{
	die("Could not connect: " . $mysqli->connect_errno . ", " . $mysql->connect_error);
}
else
{
	echo "<b> MySQL connected successfully. </b><br>";
}

// KEYPOINT_01
$uresult = $mysqli->query("SELECT * FROM animals", MYSQLI_USE_RESULT);
if ($uresult)
{
	// KEYPOINT_02
	while($row = $uresult->fetch_assoc())
	{
		echo $row['name'] . PHP_EOL;
	}
}

// KEYPOINT_03
$uresult->close();

?>

</body>
</html>