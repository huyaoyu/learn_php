<!DOCTYPE html>
<html>
<head>
	<title>015_TestErrorHandler</title>
</head>
<body>

<?php
/* In this script an error handler function will be defined.
   KEYPOINT_01: Define the error handler function.
   KEYPOINT_02: Set the error handler. */

// KEYPOINT_01
function customError($errno, $errstr)
{
	echo "<b>Error:</b> [$errno] $errstr<br />";
	echo "Ending the script.";
	die();
}

// KEYPOINT_02
set_error_handler("customError");

// trigger en error by accessing a non-exist variable.
echo $test;
?>

</body>
</html>