<!DOCTYPE html>
<html>
<head>
	<title>018_TestTryThrowCatch</title>
</head>
<body>

<?php
/* In this script, an exception will be thrown and caught. 
   KEYPOINT_01: Try block.
   KEYPOINT_02: Catch block. 
   KEYPOINT_03: Use the member function getMessage() of the Exception class. 
   KEYPOINT_04: Inherit from class Exception. 
   KEYPOINT_05: Catch a derived exception object. 
   KEYPOINT_06: Try to catch a base class exception object. 
   KEYPOINT_07: Define a top-level exception handler. 
   KEYPOINT_08: Set the top-level exception handler. 
   KEYPOINT_09: To see if the control-flow of the script continues after a top-level exception caught. */

// KEYPOINT_07
// Define a top-level exception handler
function myException($e)
{
	echo "<b>Top-Level Excption Handler: <b> ", $e->getMessage();
}

// KEYPOINT_08
// Set the top-level exception handler
set_exception_handler("myException");

// define a function to throw the exception
function checkNum($number)
{
	if ($number > 1) {
		// KEYPOINT_01
		// throw the exception
		throw new Exception("Value must be 1 or below. You specified <br />" . (string)$number, 1);
	}

	return ture;
}

function checkNum2($number)
{
	if ($number > 1) {
		// KEYPOINT_01
		// throw the exception
		throw new customException("Value must be 1 or below. You specified " . (string)$number . "<br />", 1);
	}

	return ture;
}

// KEYPOINT_01
try
{
	// trigger exception
	checkNum(2);
}

// KEYPOINT_02
catch(Exception $e)
{
	// KEYPOINT_03
	echo "Message: " . $e->getMessage();
}

// KEYPOINT_04
class customException extends Exception
{
	
	public function errorMessage()
	{
		$errorMsg = "Error in line " . $this->getLine() . " of file " . $this->getFile() . ": <b>" . $this->getMessage() . "</b><br />";
		return $errorMsg;
	}
}

try
{
	checkNum2(3);
}

// KEYPOINT_05
catch(customException $e)
{
	echo $e->errorMessage();
}

try
{
	checkNum2(4);
}

// KEYPOINT_06
// This is actually worked.
catch(Exception $e)
{
	echo $e->errorMessage();
}

// Throw an exception outside a try block. Let the top-level exception hanlder catch it.

checkNum2(5);

// KEYPOINT_09
// NOTE: The script will not execute this line.
echo "<br /><b>This is the last line.</b>";

?>

</body>
</html>