<html>
<body>

<?php
/*
This file test:
(1) The local static variable. And 
(2) The global variable that defined inside a function.
(3) The global variable and the local staic variable are diliberatly defined to have the same name.
*/


// Turn on the debug functionalities
ini_set('display_errors', 'on');
error_reporting(E_ALL | E_STRICT);

// A new variable
$x = "<p>Hello World.</p>";

// show the contents in the HTML
echo $x;

// define the local static variable
function testLocalVariable($f){
	// define a local static variable
	static $x = 0;
	$x++;
	echo "<p>local static x=",$x,"</p>";

}

// define the global variable inside a function
function testStaticGlobalVariable(){
	// global variable x defined inside function
	global $x;
	// a new local variable with the same varible name
	static $x = "<p>Hello Global Static Local Variable.</p>";
	echo $x;
}

// Define the global and local static variables
testStaticGlobalVariable();
// See the results of x as the global variable
echo $x;
// Define the local static variable x with a global variable x exist
testLocalVariable(0);
// See if the local staic variable take effect
testLocalVariable(1);

// show the contents of the global variable
echo $x;

?>

</body>
</html>
