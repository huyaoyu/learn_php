<!DOCTYPE html>
<html>
<head>
	<title>106_TestMySQL_NativeDataTypes</title>
</head>
<body>

<?php
/* This script will demonstrate hwo to convert the data in the result set into php native data types. */
/* KEYPOINT_01: Initialize a mysqli object by using mysqli_init().
   KEYPOINT_02: Set options for myqli by using options(). The option will be MYSQLI_OPT_INT_AND_FLOAT_NATIVE.
   KEYPOINT_03: Using real_connect() function.
 */

// KEYPOINT_01
$mysqli = mysqli_init();
// KEYPOINT_02
$mysqli->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, 1);
// KEYPOINT_03
$mysqli->real_connect("127.0.0.1:3306","yaoyu","essn9002","world");

if ($mysqli->connect_errno)
{
	die("Could not connect: " . $mysqli->connect_errno . ", " . $mysql->connect_error);
}
else
{
	echo "<b> MySQL connected successfully. </b><br>";
}

$res = $mysqli->query("SELECT * FROM City WHERE CountryCode=\"CHN\"");

$row = $res->fetch_assoc();

printf("<p> ID = %d (%s) </p><br>", $row["ID"], gettype($row["ID"]));
printf("<p> Name = %s (%s) </p><br>", $row["Name"], gettype($row["Name"]));

?>

</body>
</html>