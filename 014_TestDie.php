<!DOCTYPE html>
<html>
<head>
	<title>014_TestDie</title>
</head>
<body>

<?php
/* This script tests the die() function. 
   KEYPOINT_01: Use file_exists() to check if a file is exist on the file system of the sever. 
   KEYPOINT_02: Use die() function to terminate the script and show an information message on the clients browser. */

$fileName = "Welcome.txt";

// KEYPOINT_01
if (!file_exists($fileName))
{
	// KEYPOINT_02
	die("File not found: " . $fileName);
}
else
{
	$file = fopen($fileName,"r");
}

?>

</body>
</html>