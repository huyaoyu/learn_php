<!DOCTYPE html>
<html>
<head>
	<title>012_TestRequire</title>
</head>
<body>

<?php

/* This scripte will require an external php file. In that file, two variables will be defined.
   KEYPOINT_01: require a file. 
   KEYPOINT_02: use the variables defined in the file. */

// KEYPOINT_01
require "012_TestRequire_RequiredFile.php";

// KEYPOINT_02
echo "<p> I have a $color $car.</p>"; 

?>

</body>
</html>