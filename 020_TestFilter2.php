<!DOCTYPE html>
<html>
<head>
	<title>020_TestFilter2</title>
	<style>
		.error {color: #FF0000;}
	</style>
</head>
<body>

<?php

/* This script will test the filter functionalities. */
/* KEYPOINT_01: Test a groupd of strings with filter_var_array().
   KEYPOINT_02: A callback function for filtering. */

// KEYPOINT_02
function convertSpace($str)
{
	return str_replace("_", " ", $str);
}

// define the variables for the form
$nameErr = $emailErr =  $websiteErr = $commentErr = "";
$name = $email =  $website = $comment = "";
$flagAllPresent = 0;

// Obtain information from the form by POST method.
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	if (empty($_POST["name"]))
	{
		$nameErr = "Name is required.";
	}
	elseif(empty($_POST["email"]))
	{
		$emailErr = "E-mail is required.";
	}
	elseif (empty($_POST["website"]))
	{
		$websiteErr = "website is required.";
	}
	elseif (empty($_POST["comment"]))
	{
		$commentErr = "comment is required.";
	}
	else
	{
		$flagAllPresent = 1;
	}

	if ($flagAllPresent == 1)
	{
		// KEYPOINT_01
		// NOTE: The filter for variable $_POST["comment"] is a userdefined callback function.
		$filterOptions = array(
			"name"=>array("filter"=>FILTER_SANITIZE_STRING),
			"email"=>array("filter"=>FILTER_VALIDATE_EMAIL),
			"website"=>array("filter"=>FILTER_SANITIZE_URL),
			"comment"=>array("filter"=>FILTER_CALLBACK,"options"=>"convertSpace")
			);

		// KEYPOINT_01
		$results = filter_input_array(INPUT_POST, $filterOptions);

		if (!$results["name"])
		{
			echo "<b>name is invalid.</b><br />";
		}
		elseif (!$results["email"])
		{
			echo "<b>emial is invalid.</b><br />";
		}
		elseif (!$results["website"])
		{
			echo "<b>website is invalid.</b><br />";
		}
		elseif (!$results["comment"])
		{
			echo "<b>comment is invalid.</b> <br />";
		}
		else
		{
			echo "<b>The user inputs are valid.</b><br />";
			$name    = $results["name"];
			$email   = $results["email"];
			$website = $results["website"];
			$comment = $results["comment"];
		}
	}
}

?>

<!-- The form. -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	Name: <input type="text" name="name" value="<?php echo $name; ?>">
	<span class="error">* <?php echo $nameErr; ?> </span>
	<br><br>
	E-mail: <input type="text" name="email" value="<?php echo $email; ?>">
	<span class="error">* <?php echo $emailErr; ?></span>
	<br><br>
	Website: <input type="text" name="website" value="<?php echo $website; ?>">
	<span class="error">* <?php echo $websiteErr; ?></span>
	<br><br>
	<label>Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment; ?></textarea>
	<span class="error">* <?php echo $commentErr; ?></span>
	<br><br>
	<br><br>
	<input type="submit" name="submit" value="Submit here">
</form>

<?php 

echo "<h2> Your input is: </h2>";
echo "name = ", $name, "<br>";
echo "email = ", $email, "<br>";
echo "website = ", $website, "<br>";
echo "comment = ", $comment, "<br>";

?>

</body>
</html>