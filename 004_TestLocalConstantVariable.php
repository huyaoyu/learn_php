<html>
<body>

<?php
/*
This file will test the constant value.
A constant x will be dedined in a function. And another constant x will be defined outside the function.
*/

ini_set('display_errors', 'on');
error_reporting(E_ALL | E_STRICT);


// function defines the constant
function defineConstantX(){
	define('x','Hello World!',false);
}

// define the constant inside a function
defineConstantX();

// show the content of the constant
echo x;

// try to define the constant with the same name
// NOTE: This will triger an error in php.
define('x','Hello World 2!',false);

// show the content of the constant
echo x;

?>

</body>
</html>
