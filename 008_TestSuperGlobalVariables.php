<!DOCTYPE html>
<html>
<head>
	<title>008_TestSuperGlobalVariables</title>
</head>
<body>

<?php
/* This file test the super global variables */

function writeHTMLP($str)
{
	echo "<p>", $str, "</p>";
}

// The two global varialbes
$x = 75;
$y = 25;

// ********* Test the $GLOBALS variable

function addition(){
	$GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];
}

addition();

echo "<p>", "z = ", $z, "</p>";

// ************* Test the $_SERVER variable

echo "<p>\$_SERVER[\"PHP_SELF\"] = ",            $_SERVER["PHP_SELF"], "</p>";
echo "<p>\$_SERVER[\"GATEWAY_INTERFACE\"] = ",   $_SERVER["GATEWAY_INTERFACE"], "</p>";
echo "<p>\$_SERVER[\"SERVER_ADDR\"] = ",         $_SERVER["SERVER_ADDR"], "</p>";
echo "<p>\$_SERVER[\"SERVER_NAME\"] = ",         $_SERVER["SERVER_NAME"], "</p>";
echo "<p>\$_SERVER[\"SERVER_SOFTWARE\"] = ",     $_SERVER["SERVER_SOFTWARE"], "</p>";
echo "<p>\$_SERVER[\"SERVER_PROTOCOL\"] = ",     $_SERVER["SERVER_PROTOCAL"], "</p>";
echo "<p>\$_SERVER[\"REQUEST_METHOD\"] = ",      $_SERVER["REQUEST_METHOD"], "</p>";
echo "<p>\$_SERVER[\"REQUEST_TIME\"] = ",        $_SERVER["REQUEST_TIME"], "</p>";
echo "<p>\$_SERVER[\"QUERY_STRING\"] = ",        $_SERVER["QUERY_STRING"], "</p>";
echo "<p>\$_SERVER[\"HTTP_ACCEPT\"] = ",         $_SERVER["HTTP_ACCEPT"], "</p>";
echo "<p>\$_SERVER[\"HTTP_ACCEPT_CHARSET\"] = ", $_SERVER["HTTP_ACCEPT_CHARSET"], "</p>";
echo "<p>\$_SERVER[\"HTTP_HOST\"] = ",           $_SERVER["HTTP_HOST"], "</p>";
echo "<p>\$_SERVER[\"HTTP_REFERER\"] = ",        $_SERVER["HTTP_REFERER"], "</p>";
echo "<p>\$_SERVER[\"HTTPS\"] = ",               $_SERVER["HTTPS"], "</p>";
echo "<p>\$_SERVER[\"REMOTE_ADDR\"] = ",         $_SERVER["REMOTE_ADDR"], "</p>";
echo "<p>\$_SERVER[\"REMOTE_HOST\"] = ",         $_SERVER["REMOTE_HOST"], "</p>";
echo "<p>\$_SERVER[\"REMOTE_PORT\"] = ",         $_SERVER["REMOTE_PORT"], "</p>";
echo "<p>\$_SERVER[\"SCRIPT_FILENAME\"] = ",     $_SERVER["SCRIPT_FILENAME"], "</p>";
echo "<p>\$_SERVER[\"SERVER_ADMIN\"] = ",        $_SERVER["SERVER_ADMIN"], "</p>";
echo "<p>\$_SERVER[\"SERVER_PORT\"] = ",         $_SERVER["SERVER_PORT"], "</p>";
echo "<p>\$_SERVER[\"SERVER_SIGNATURE\"] = ",    $_SERVER["SERVER_SIGNATURE"], "</p>";
echo "<p>\$_SERVER[\"PATH_TRANSLATED\"] = ",     $_SERVER["PATH_TRANSLATED"], "</p>";
echo "<p>\$_SERVER[\"SCRIPT_NAME\"] = ",         $_SERVER["SCRIPT_NAME"], "</p>";
echo "<p>\$_SERVER[\"SCRIPT_URI\"] = ",          $_SERVER["SCRIPT_URI"], "</p>";

?>

</body>
</html>