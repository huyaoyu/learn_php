<!DOCTYPE html>
<html>
<head>
	<title>104_TestMySQL_NavigationThroughBufferedResults</title>
</head>
<body>

<?php
/* This script will demonstrate the usage of navigation through buffered results. */
/* KEYPOINT_01: Use data_seek() function to navigation.
 */

$mysqli = new mysqli("127.0.0.1:3306","yaoyu","essn9002","world");

if ($mysqli->connect_errno)
{
	die("Could not connect: " . $mysqli->connect_errno . ", " . $mysql->connect_error);
}
else
{
	echo "<b> MySQL connected successfully. </b><br>";
}

$res = $mysqli->query("SELECT * FROM City WHERE CountryCode=\"CHN\"");

echo "<p>Reverse order ...</p><br>";

for ($row_no = $res->num_rows - 1; $row_no >= 0; $row_no--)
{
	// KEYPOINT_01
	$res->data_seek($row_no);
	$row = $res->fetch_assoc();
	echo " ID = " . $row["ID"] . ", Name = " . $row["Name"] . "<br>";
}

?>

</body>
</html>