<!DOCTYPE html>
<html>
<head>
	<title>TestArrayKeyValuePair</title>
</head>
<body>

<?php
/*This file tests the key-value pair of array.*/

// Create an array in key-value manner
$a = array('Bill' => 1, 'James' => 2);

// Use the foreach syntax

foreach ($a as $k => $v) {
	echo '<p>', $k, ' => ', $v, '</p>';
}


?>

</body>
</html>