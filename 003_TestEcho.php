<html>
<body>

<?php
/*
This file will test different styles of echo instructions.
*/

// A new variable
$x = "<p>Hello World 2.</p>";

// echo with different styles
echo $x;
echo "$x";
echo "{$x}";

?>

</body>
</html>
