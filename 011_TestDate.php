<!DOCTYPE html>
<html>
<head>
	<title>011_TestDate</title>
</head>
<body>

<?php

/* KEYPOINT_01: Set the default time zone for this script.
   KEYPOINT_02: Get the date information.
   KEYPOINT_03: Get the time information. */

// KEYPOINT_01
/* The valid asia time zones are lited in
php.net/manual/en/timezones.asia.php */
date_default_timezone_set("Asia/Shanghai");

// KEYPOINT_02
echo "Today is " . date("Y/m/d"). "<br>";
echo "Today is " . date("Y.m.d"). "<br>";
echo "Today is " . date("Y-m-d"). "<br>";
echo "Today is " . date("1"). "<br>";

// KEYPOINT_03
echo "The time is " . date("h:i:sa");

?>

</body>
</html>