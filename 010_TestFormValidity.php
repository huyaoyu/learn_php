<!DOCTYPE html>
<html>
<head>
	<title>010_TestFormValidity</title>
	<!-- KEYPOINT_04 -->
	<style>
		.error {color: #FF0000;}
	</style>
</head>
<body>

<!-- This file creates a form. The form is transfered by post method. -->
<!-- This file is also resposible for check the validity of the form. -->

<!-- KEYPOINT_01: The check-validity code should be in front of the code of the form. -->
<!-- KEYPOINT_02: Use empty() to check if the form has empty entry. -->
<!-- KEYPOINT_03: Use trim(), stripslashes() and htmlspecialchars() to process the input strings.-->
<!-- KEYPOINT_04: There is a <style> element in the <head>,
the error style is used in the <span> element in the form. -->
<!-- KEYPOINT_05: Use preg_match() to check the name entry with reg-expression -->
<!-- KEYPOINT_06: Use preg_match() to check the emial entry with reg-expression -->
<!-- KEYPOINT_07: Use preg_match() to check the website entry with reg-expression -->
<!-- KEYPOINT_08: Let the form keep the content after the form is submitted. -->


<!-- KEYPOINT_01 -->
<!-- The validity-check code. -->
<?php
// define the variables
$nameErr = $emailErr = $genderErr = $websiteErr = $commentErr = "";
$name = $email = $gender = $comment = $website = "";

// Obtain information from the form by POST method.
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	// KEYPOINT_02
	if (empty($_POST["name"]))
	{
		$nameErr = "Name is required.";
	}
	else
	{
		$name = testInput($_POST["name"]);
		// KEYPOINT_05
		if (!preg_match("/^[a-zA-Z]*$/", $name))
		{
			$nameErr = "Only english characters and sapces are allowed.";
		}
	}
	
	if (empty($_POST["email"]))
	{
		$emailErr = "E-mail is required.";
	}
	else
	{
		$email   = testInput($_POST["email"]);
		// KEYPOINT_06
		if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
		{
			$emailErr = "Invalid email address.";
		}
	}
	
	if (empty($_POST["website"]))
	{
		$websiteErr = "";
	}
	else
	{
		$website = testInput($_POST["website"]);
		// KEYPOINT_07
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~|!:,.;]*[-a-z0-9+&@#\/%?=~|]/i",$website))
		{
			$websiteErr = "Invalid website address.";
		}
	}
	
	if (empty($_POST["comment"]))
	{
		$commentErr = "";
	}
	else
	{
		$comment = testInput($_POST["comment"]);
	}

	if (empty($_POST["gender"]))
	{
		$genderErr = "Gender is required.";
	}
	else
	{
		$gender  = testInput($_POST["gender"]);
	}
}

// Define the testInput() function.
function testInput($data)
{
	// KEYPOINT_03
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

?>

<!-- The form. -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	<!-- KEYPOINT_08  use the value property -->
	Name: <input type="text" name="name" value="<?php echo $name; ?>">
	<!-- KEYPOINT_04 -->
	<span class="error">* <?php echo $nameErr; ?> </span>
	<br><br>
	E-mail: <input type="text" name="email" value="<?php echo $email; ?>">
	<span class="error">* <?php echo $emailErr; ?></span>
	<br><br>
	Website: <input type="text" name="website" value="<?php echo $website; ?>">
	<span class="error"> <?php echo $websiteErr; ?></span>
	<br><br>
	<label>Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment; ?></textarea>
	<br><br>
	Gender:
	<!-- KEYPOINT_08, use the checked property -->
	<input type="radio" name="gender" 
	<?php if (isset($gender) && $gender=="female") echo "checked"; ?>
	value="female">Female
	<input type="radio" name="gender"
	<?php if (isset($gender) && $gender=="male") echo "checked"; ?>
	value="male">Male
	<span class="error">* <?php echo $genderErr; ?></span>
	<br><br>
	<input type="submit" name="submit" value="Submit here">
</form>

<?php 

echo "<h2> Your input is: </h2>";
echo $name, "<br>";
echo $email, "<br>";
echo $website, "<br>";
echo $comment, "<br>";
echo $gender, "<br>";

?>

</body>
</html>