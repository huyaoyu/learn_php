<!DOCTYPE html>
<html>
<head>
	<title>019_TestFilter</title>
	<style>
		.error {color: #FF0000;}
	</style>
</head>
<body>

<?php

/* This script will test the filter functionalities. */
/* KEYPOINT_01: Use filter_has_var() to test the existance of a variable. 
   KEYPOINT_02: Use filter_var() to check an integer. In this case, the integer is valid.
   KEYPOINT_03: Use filter_var() to check an integer. In this case, the integer is invalid.
   KEYPOINT_04: Supply arguments for filter_var(). 
   KEYPOINT_05: Use filter_input() to check en email address. 
   KEYPOINT_06: Use filter_input() to sanitize a url string, which is obtained from the form.*/

$integer_00 = 123;
$integer_01 = 321;

// KEYPOINT_04
$intFilterOptions = array("options"=>array("min_range"=>0,"max_range"=>256));

// KEYPOINT_02
if (!filter_var($integer_00,FILTER_VALIDATE_INT, $intFilterOptions))
{
	echo "Integer_00 is not valid.<br />";
}
else
{
	echo "Integer_00 is valid.<br />";
}

// KEYPOINT_03
if (!filter_var($integer_01,FILTER_VALIDATE_INT, $intFilterOptions))
{
	echo "Integer_01 is not valid.<br />";
}
else
{
	echo "Integer_01 is valid.<br />";
}

// define the variables for the form
$nameErr = $emailErr =  $websiteErr = "";
$name = $email =  $website = "";

// Obtain information from the form by POST method.
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	// KEYPOINT_01
	if (!filter_has_var(INPUT_POST, "name2"))
	{
		echo "<b>name2 does not exist.<b><br />";
	}

	if (empty($_POST["name"]))
	{
		$nameErr = "Name is required.";
	}
	else
	{
		$name = testInput($_POST["name"]);
		if (!preg_match("/^[a-zA-Z]*$/", $name))
		{
			$nameErr = "Only english characters and sapces are allowed.";
		}
	}
	
	if (empty($_POST["email"]))
	{
		$emailErr = "E-mail is required.";
	}
	else
	{
		$email   = testInput($_POST["email"]);
		// KEYPOINT_05
		if (!filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL))
		{
			$emailErr = "Invalid email address.";
		}
		else
		{
			echo "The email is valid.<br />";
		}

	}
	
	if (empty($_POST["website"]))
	{
		$websiteErr = "";
	}
	else
	{
		if(!filter_has_var(INPUT_POST, "website"))
		{
			$websiteErr = "The website variable does not exist.";
		}
		else
		{
			// KEYPOINT_06
			// The user shold input a invalid url.
			// It can be achieved by input a url with chinese characters.
			$website = filter_input(INPUT_POST, "website", FILTER_SANITIZE_URL);
		}
	}
}

// Define the testInput() function.
function testInput($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

?>

<!-- The form. -->
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
	Name: <input type="text" name="name" value="<?php echo $name; ?>">
	<span class="error">* <?php echo $nameErr; ?> </span>
	<br><br>
	E-mail: <input type="text" name="email" value="<?php echo $email; ?>">
	<span class="error">* <?php echo $emailErr; ?></span>
	<br><br>
	Website: <input type="text" name="website" value="<?php echo $website; ?>">
	<span class="error"> <?php echo $websiteErr; ?></span>
	<br><br>
	<br><br>
	<input type="submit" name="submit" value="Submit here">
</form>

<?php 

echo "<h2> Your input is: </h2>";
echo "name = ", $name, "<br>";
echo "email = ", $email, "<br>";
echo "website = ", $website, "<br>";
echo "integer_00 = ", $integer_00, "<br>";
echo "integer_01 = ", $integer_01, "<br>";

?>

</body>
</html>