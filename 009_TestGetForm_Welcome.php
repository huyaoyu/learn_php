<!DOCTYPE html>
<html>
<head>
	<title>009_TestGetForm_Welcome</title>
</head>

<!-- This HTML file contains the php code for handle the form in 009_TestGetForm.html. -->
<!-- The $_GET global variable will be used. -->

<body>

Welcome <?php echo $_GET["name"]; ?><br>
Your email address is: <?php echo $_GET["email"]; ?><br>

</body>
</html>