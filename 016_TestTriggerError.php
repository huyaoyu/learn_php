<!DOCTYPE html>
<html>
<head>
	<title>016_TestTriggerError</title>
</head>
<body>

<?php
/* This script will trigger an error with error level.
   KEYPOINT_01: Trigger a error by using trigger_error() */

// defin ethe error handler
function customError($errno, $errstr)
{
	echo "<b>Error:</b> [$errno] $errstr<br />";
	echo "Ending the script.";
	die();
}

// set the error handler
set_error_handler("customError");

// define a variable
$test = 1;

if ($test < 2)
{
	// KEYPOINT_01
	// E_USER_WARNING is the error level.
	trigger_error("The value is less than 2.", E_USER_WARNING);
}

?>

</body>
</html>