<html>
<body>

<?php
/*
This file will test the usage of the logic operators for array objects.
*/


// turn on the debug functionalities
ini_set('display_errors', 'on');
error_reporting(E_ALL | E_STRICT);


// The arrays to be compared
$a1 = array("a" => 0, "b" => 1);
$a2 = array("a" => 0, "b" => 1); // exact with $a1
$a3 = array("b" => 1, "a" => 0); // same key-values, but diffenent in sequence
$a4 = array("a" => 0, "b" => 0); // with one different key-value pair
$a5 = array("a" => 1, "b" => 0); // different key-value pairs

// show the contents of the arrays

echo "<p>The arrays to be compared:</p><br>";
echo "<p>a1 = </p>";
var_dump($a1);
echo "<p>a2 = </p>";
var_dump($a2);
echo "<p>a3 = </p>";
var_dump($a3);
echo "<p>a4 = </p>";
var_dump($a4);
echo "<p>a5 = </p>";
var_dump($a5);
// Test the comparison

echo "<p>The results of comparison:</p><br>";

echo "<p>a1 == a2</p>";
var_dump($a1 == $a2);

echo "<p>a1 == a3</p>";
var_dump($a1 == $a3);

echo "<p>a1 != a3</p>";
var_dump($a1 != $a3);

echo "<p>a1 <> a3</p>";
var_dump($a1 <> $a3);

echo "<p>a1 === a2</p>";
var_dump($a1 === $a2);

echo "<p>a1 === a3</p>";
var_dump($a1 === $a3);

echo "<p>a1 !== a2</p>";
var_dump($a1 !== $a2);

echo "<p>a1 !== a3</p>";
var_dump($a1 !== $a3);

echo "<p>a1 !== a4</p>";
var_dump($a1 !== $a4);

echo "<p>a1 !== a5</p>";
var_dump($a1 !== $a5);
?>

</body>
</html>
