<!DOCTYPE html>
<html>
<head>
	<title>105_TestMySQL_NavigationThroughUnbufferedResults</title>
</head>
<body>

<?php
/* This script will demonstrate the usage of navigation through unbuffered results. */
/* KEYPOINT_01: Use reql_query() function to do an unbuffered query.
   KEYPOINT_02: Use use_result() function to get a resource handle of the result set.
   KEYPOINT_03: Use fetch_assoc() function to navigation.
 */

$mysqli = new mysqli("127.0.0.1:3306","yaoyu","essn9002","world");

if ($mysqli->connect_errno)
{
	die("Could not connect: " . $mysqli->connect_errno . ", " . $mysql->connect_error);
}
else
{
	echo "<b> MySQL connected successfully. </b><br>";
}

// KEYPOINT_01
$mysqli->real_query("SELECT * FROM City WHERE CountryCode=\"CHN\"");
// KEYPOINT_02
$res = $mysqli->use_result();

echo "<p>Fetch from remote server one by one ...</p><br>";

// KEYPOINT_03
while ($row = $res->fetch_assoc())
{
	echo " ID = " . $row["ID"] . ", Name = " . $row["Name"] . "<br>";
}

?>

</body>
</html>