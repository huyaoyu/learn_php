<!DOCTYPE html>
<html>
<head>
	<title>017_TestUncaughtException</title>
</head>
<body>

<?php
/* In this script, an exception will be thrown but it will not be caught. Failing to catch an exception will cause a fatal error. 
   KEYPOINT_01: throw an exception.
   KEYPOINT_02: trigger an exceptiong.*/

// define a function to throw the exception
function checkNum($number)
{
	if ($number > 1) {
		// KEYPOINT_01
		// throw the exception
		throw new Exception("Value must be 1 or below", 1);
	}

	return ture;
}

// KEYPOINT_02
// trigger exception
checkNum(2);

?>

</body>
</html>