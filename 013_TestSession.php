<?php session_start(); ?>
<!-- KEYPOINT_01 -->
<!-- If you do not do this, two warning will appear on the web page.
The solution is found on
http://stackoverflow.com/questions/8812754/cannot-send-session-cache-limiter-headers-already-sent -->

<!-- KEYPOINT_01: Put the specific line at the very beginning of the php file to start a session. -->
<!-- KEYPOINT_02: Use isset() function to test if a variable exists. -->
<!-- KEYPOINT_03: Accumulate the $_SESSION["views"] variable. -->
<!-- KEYPOINT_04: A new key-value pair will be created at the first access operation. -->


<?php

// KEYPOINT_02
if (isset($_SESSION["views"]))
{
	// KEYPOINT_03
	$_SESSION["views"] = $_SESSION["views"] + 1;
}
else
{
	// KEYPOINT_04
	$_SESSION["views"] = 1;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>013_TestSession</title>
</head>
<body>

<?php echo "<p> views =", $_SESSION["views"], "</p>"; ?>

</body>
</html>