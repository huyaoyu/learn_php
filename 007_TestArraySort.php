<!DOCTYPE html>
<html>
<head>
	<title>007_TestArraySort</title>
</head>
<body>

<?php
/* This file test the functions which sort the array */

// define a function to echo a line in <p></p> html element

function writeHTMLP($str)
{
	echo "<p>", $str, "</p>";
}

// define a function to display the array
function dispArray($a)
{
	foreach ($a as $k => $v) {
		// echo the key-value pair
		echo '<p>', $k, " => ", $v, "</p>";
	}
}

// Create the array

$cars = array("Volvo", "BMW", "SAAB");

// Show the content of the original array
echo  "<p> The array is: </p>";
dispArray($cars);

// test sort()
sort($cars);
writeHTMLP("After sort():");
dispArray($cars);

// test rsort(), reverse sort()
rsort($cars);
writeHTMLP("After rsort():");
dispArray($cars);

// Create another array for testing
$age = array("Bill" => "35", "Peter" => "43", "Steve" => "37");
writeHTMLP("Create a new array:");
dispArray($age);

// Test asort(), sort by value
asort($age);
writeHTMLP("After asort():");
dispArray($age);

// Test ksort(), sort by key
ksort($age);
writeHTMLP("After ksort():");
dispArray($age);

// Test arsort(), reverse sort by value
arsort($age);
writeHTMLP("After arsort():");
dispArray($age);

// Test krsort(), reverse sort by key
krsort($age);
writeHTMLP("After krsort():");
dispArray($age);

?>

</body>
</html>