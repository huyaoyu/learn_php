<html>
<body>

<?php
/*
This file test the ability for php to assign values of different type to the same variable.
*/

// A new variable
$x = 1;                        // The first type, an integer
$x = "<p>Hello World 2.</p>"; // The second type, an string


echo $x;

?>

</body>
</html>
